/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.pragma.test;
import static spark.Spark.before;
import static spark.Spark.get;

/**
 *
 * @author gustavo.rodriguez
 */
public class SparkResources {

    public static void defineResources() {
        before((request, response) -> response.type("application/json"));

        get("/prueba",(req, res) -> {
            return "Hello from lambda by Java";
        });
        
        get("/pruebax",(req, res) -> {
            return "Hello from lambda by Java";
        });
        
//        post("/pets", (req, res) -> {
//            Pet newPet = LambdaContainerHandler.getObjectMapper().readValue(req.body(), Pet.class);
//            if (newPet.getName() == null || newPet.getBreed() == null) {
//                return "Error";
//            }
//
//            Pet dbPet = newPet;
//            dbPet.setId(UUID.randomUUID().toString());
//
//            res.status(200);
//            return dbPet;
//        }, new JsonTransformer());
    }
}
