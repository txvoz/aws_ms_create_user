/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.pragma.test.utils;

import com.google.gson.Gson;
import spark.ResponseTransformer;

/**
 *
 * @author gustavo.rodriguez
 */
public class JsonTransformer implements ResponseTransformer {
    private Gson gson = new Gson();
   
    @Override
    public String render(Object model) {
        return gson.toJson(model);
    }

}
